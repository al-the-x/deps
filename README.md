# `deps` -- Simple dependency injection provider

A simple dependency injector should allow arbitrary services to be registered and retrieved with as little fuss as possible, conforming to the prevailing idioms of the language in which it is implemented, and devoid of "magic" or edge-cases that make it's use inconsistent depending on context.

```js
import deps from 'deps'

// register deps synchronously, always with a callback:
deps.register('API_BASE_URL', deps => '/api')

// Always get deps _asynchronously_, ideally with `await`:
console.assert(await deps.API_BASE_URL === '/api')
```

I should be able to register `deps` in any order:

```js
// ApiService is dependent on `axios`:
deps.register('ApiService', deps =>
  class ApiService {
    // So constructor accepts `deps`:
    async constructor (deps) {
      // And requests from the Injector:
      this.client = await deps.get('axios')
    }
    // . . .
  }
)

// Then we register `axios` afterwards:
deps.register('axios', async deps => {
  const axios = require('axios') // or `await import('axios')`
  
  // But _before_ registering `AXIOS_CONFIG`:
  return axios.create(await deps.get('AXIOS_CONFIG'))
}

// Registering `deps` out of order should be fine;
// None of these callbacks are executed yet.
deps.register('AXIOS_CONFIG', deps => ({
  baseURL: deps.get('API_BASE_URL')
})
```

~~Eschew verbosity whensoever possible~~ Write plainly:

```js
// Typing `register` might get old
deps('TheApp', async deps => {
  // nothing to config; don't over-do it
  const TheApp = await import('./TheApp.vue')
 
  // just return the config, because instantiating 
  // `Vue` has side-effects outside of DI.
  return {
    el: '#the-app',
    // leverage other DIs like `Vue.provide` / `React.context`:
    provide: {
      // instantiate a Singleton
      API: await deps.singleton('ApiService'),
      deps, // very meta
    },
    ...TheApp
  }
})

// This might seem like overkill but `Vue.use`:
deps('Vue', async deps => {
  const Vue = await import('vue')
  
  // remember, always async access!
  Vue.use(await deps('Vuex'))
  
  return Vue
})
```

Where is the _bottom_ of this chain of `deps` anyway?

```js
/**
 * In `src/main.js`, invoke `deps.main(callback)` to `resolve`
 * all `deps` and invoke the supplied `callback` with all the
 * resolved `deps`.
 */
deps.main(deps => { // or shorthand with `deps(callback)`
  // Now `deps` are `resolved`, so destructuring works:
  const { Vue, TheApp } = deps
  
  return new Vue(TheApp) // Side-effects welcomed!
}).catch(e => console.error(e))
```

Or if you prefer to `resolve` them yourself without the `callback`:

```js
try {
    const { Vue, TheApp } = await deps.resolve()

    new Vue(TheApp)
} catch (e) {
    console.error(e)
}
```

But don't try to `register` after calling `resolve` or `main`:

```js
deps.resolve()

// will throw because new `deps` cannot be added after `resolve`...
deps.register('anything', deps => ({}))
```

The `resolve` method is _almost_ Idempotent:

```js
const [ A, B ] = [
  await deps.resolve(),
  await deps.resolve()
]

console.assert( ! Object.is(A, B) ) // they're clones
console.assert(Object.is(
  JSON.stringify(A),
  JSON.stringify(B)
)) // but identical in content
```

> Yes, I know that `JSON.stringify` is not the same as `deepEqual`. Roll with it. Don't `@` me.

Register as many `mainFn`s as you want, but beware:

```js
// Queues a Promise to resolve on the next micro-tick...
const main = deps.main(deps => {
  throw new Error('asplode!')
})

// Shorthand for `deps.main(deps => { })` again:
await deps(deps => { 
  console.log('not asploding')
})

try {
  await waiting
} catch (e) {
  console.assert(e.message === 'asplode!')
}
```