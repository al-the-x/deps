import test from 'ava'
import beforeEach from './helpers.beforeEach'
import { CircularDependency } from '../deps'

test.beforeEach(beforeEach)
test.serial.beforeEach(t => {
  t.context = {
    childOf: t.context.deps.$$childOf,
    resolved: new Map,
    parents: [ ],
    child: 'foo',
  }
})

test('Basics: we setup the `context` correctly', t => {
  const { childOf, resolved, parents, child } = t.context

  t.assert(childOf instanceof Function,
    'since we plucked `$$childOf` from `deps`')

  t.assert(
    resolved instanceof Map &&
    parents instanceof Array &&
    child === 'foo',
    'since they are assigned in `beforeEach`'
  )
})

test('Very Easy: `parents` is empty: expect `false`', childOf__returns, {
  expected: { value: false },
  message: 'since there are no `parents`',
})

test('Easy: `parents` ONLY contains "foo": throw', childOf__throws, {
  fixtures: { parents: [ 'foo' ] },
  expected: { message: 'Circular dependency: foo -> foo' },
})

test('Easy: `parents` contains "foo": throw', childOf__throws, {
  fixtures: { parents: [ 'foo', 'bar', 'baz' ] },
  expected: { message: 'Circular dependency: foo -> bar -> baz -> foo' },
})

/**
 * Circular dependency: bar -> baz -> foo -> bar
 */
test('Harder: "foo" depends on something in `parents`: throw', childOf__throws, {
  fixtures: {
    // checking `childOf` starting at "bar", which depends on
    parents: ['bar', 'baz'],
    resolved: new Map([
      ['foo', new Set(['bar'])],
      ['bar', new Set(['baz'])],
      ['baz', new Set()],
    ]),
  },
  expected: { message: 'Circular dependency: bar -> baz -> foo -> bar' },
  message: 'since "foo" depends on "baz" indirectly'
})

/**
 * Because "foo" has been partially resolved to depend on "bat", and "bat"
 * has been partially resolved to depend on "baz", and we're testing that "foo"
 * is a valid child of "baz":
 *
 * Circular dependency: baz (B) -> foo (C) -> bat (D) -> baz (B)
 *
 *      A: quux
 *      |
 *  +-> B: baz --> E: bar
 *  |   |
 *  ↑   C: foo <-- considering "foo" as `childOf` "baz"
 *  |   |
 *  +-- D: bat
 *
 *  This is possible because the children are resolved by Promise in a non-
 *  deterministic fashion. Dep "foo" may still have deps to resolve, but we
 *  haven't determined that yet. So we operate from the information we have.
 */
test('HARDERER: "foo" depends on "bat" which depends on "baz": throw', childOf__throws, {
  fixtures: {
    parents: ['quux', 'baz'],
    resolved: new Map([
      // "foo" is known to depend on "bat"
      ['foo', new Set(['bat'])],
      // "bat" is known to depend on "baz"
      ['bat', new Set(['baz'])],
      // "bar" is known to depend on "baz"
      ['bar', new Set(['baz'])],
      // "baz" is known to depend on "quux"
      ['baz', new Set(['bar'])],
    ]),
  },
  expected: { message: 'Circular dependency: baz -> foo -> bat -> baz' },
  message: 'since `baz -> foo -> bat -> baz`',
})

function childOf__returns(t, { fixtures, expected, message }={}) {
  const { childOf, resolved, parents, child } = {
    ...t.context, ...fixtures
  }

  t.assert(childOf(resolved, parents, child) === expected.value, message)

  t.assert(resolved.has(child) && resolved.get(child) instanceof Set,
    'since `childOf` ensures `child` is in `resolved`')

  expected.resolved && t.deepEqual(resolved, expected.resolved,
    'since `childOf` updates `resolved` for `parents` and `child`')
}

function childOf__throws(t, { fixtures, expected, message }) {
  const { childOf, resolved, parents, child } = {
    ...t.context, ...fixtures
  }

  t.throws(() => childOf(resolved, parents, child),
    { instanceOf: CircularDependency, ...expected },
    message
  )
}
