import deps from '../deps'

beforeEach.title = () => 'Provide isolated `deps` injector'
export default async function beforeEach(t){
  t.context = { deps: deps.detach() }
}
