import test from 'ava'
import beforeEach from './helpers.beforeEach'

import { CircularDependency } from '../deps'

test.beforeEach(beforeEach)

test('Simple Circular Dependency: A -> B -> A', async t => {
  const { deps } = t.context

  deps
    .register('A', async deps => await deps.get('B'))
    .register('B', async deps => await deps.get('A'))

  await t.throwsAsync(() => deps('A'), {
    instanceOf: CircularDependency,
    message: 'Circular dependency: A -> B -> A'
  })
})

test('Longer Circular Dependency: A -> B -> C -> D -> A', async t => {
  const { deps } = t.context

  ;['A', 'B', 'C', 'D', 'A'].reduce((left, right) => (
    deps(left, deps => deps(right)) && right
  ))

  await t.throwsAsync(() => deps.resolve(), CircularDependency)
})

/**
 * The Deadly Diamond Dependency Graph
 *
 * This should be fine. "A" will resolve "B" and "C". Either "B" or "C" will
 * resolve "D".  If "D" is not a singleton, "B" and "C" will get different
 * copies of "D"... As intended.
 *
 *     A
 *    / \
 *   B   C
 *    \ /
 *     D
 */
test('The Deadly Diamond Dependency Graph', async t => {
  const { deps } = t.context

  const diamond = deps
    .register('A', async deps => await deps.get(['B', 'C']))
    .register('B', async deps => await deps.get('D'))
    .register('C', async deps => await deps.get('D'))
    .register('D', async deps => 'done')

  t.deepEqual(await diamond.resolve(), {
    A: ['done', 'done'],
    B: 'done',
    C: 'done',
    D: 'done'
  })
})

/**
 * But what if... "B" depends on "C", but "C" depends on "B"!?
 *
 *    A
 *   / \
 *  B<->C <- !!! Ruh Roh, Raggy!
 *   \ /
 *    D
 *
 * That should totally explode: "A" will resolve "B", which will attempt to
 * resolve "C", which will attempt to resolve "B"... Circular dependency!
 */
test('Circular Dependency Hell! A -> [ B -> [ C, D ], C -> [ B, D ] ]', async t => {
  const { deps } = t.context

  const cross = deps
    .register('A', deps => deps.get(['B', 'C']))
    .register('B', deps => deps.get(['C', 'D']))
    .register('C', deps => deps.get(['B', 'D']))
    .register('D', deps => '-{-batwaldo-}-')

  await t.throwsAsync(() => cross.resolve(), {
    instanceOf: CircularDependency,
    // the resolution order isn't strictly deterministic
    message: /(B -> C -> B|C -> B -> C)/
  })
})
