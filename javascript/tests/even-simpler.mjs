import test from 'ava'
import beforeEach from './helpers.beforeEach'

test.beforeEach(beforeEach)

test("But wait, there's an EVEN SIMPLER API!", async t => {
  const { deps } = t.context

  // Pass `name` and `dep` callback to `register` a dep...
  deps('quux', deps => 'waldo')

  deps('foo', deps => {
    // Pass just `name` to `get` a dep...
    return deps('waldo')
  })

  // Or `await` to do something interesting with it...
  deps('bar', async deps => {
    const parts = await deps('baz')

    return parts.join('')
  })

  // Passing multiple `name` values is supported...
  deps('baz', deps => deps(['bat', 'quux']))

  // Registering over an existing dep isn't supported...
  t.throws(() => deps('foo', deps => 'already registered'))

  // Use `replace` or `decorate` explicitly...
  t.notThrows(() => {
    deps.replace('foo', deps => deps('bar'))

    deps.decorate('foo', (deps, original) => `-{-${original}-}-`)
  })

  // You still can't `get` an unregistered dep...
  t.throwsAsync(() => deps('A'), /not registered/)

  // Please don't send a list of names to `register`...
  t.throws(() => deps(['A', 'B'], deps => 'what are you even doing?'),
    /not supported/)

  // Don't forget any deps!
  deps('bat', deps => 'bat')

  // Pass nothing to `resolve`...
  t.deepEqual(await deps(), {
    foo: '-{-batwaldo-}-',
    bar: 'batwaldo',
    baz: ['bat', 'waldo'],
    bat: 'bat',
    quux: 'waldo'
  })

  // Or pass a `mainFn` instead.
  await deps(({ foo }) => {
    t.assert(foo, '-{-batwaldo-}-') // returns!
  })
})


