import test from "ava"
import beforeEach from './helpers.beforeEach'

test.beforeEach(beforeEach)

test('I can `register` deps ONCE and `get` deps later', async t => {
  const { deps } = t.context

  t.assert(deps.API_BASE_URL === undefined)

  t.assert(deps.register instanceof Function)

  const API_BASE_URL = "/api"

  t.assert(deps.register("API_BASE_URL", deps => API_BASE_URL) === deps)

  t.assert(await deps.get("API_BASE_URL"), API_BASE_URL)

  t.throws(
    () => deps.register("API_BASE_URL", API_BASE_URL),
    "API_BASE_URL is already registered"
  )
})

test('I can `get` multiple deps in the same call', async t => {
  const { deps } = t.context

  const resolved = deps
    .register('A', async deps => await deps.get(['B', 'C']))
    .register('B', deps => 'B')
    .register('C', deps => 'C')
    .resolve()

  t.deepEqual(await resolved, {
    A: ['B', 'C'],
    B: 'B',
    C: 'C',
  })
})

test('I can `register` and `get` a graph of deps', async t => {
  const { deps } = t.context

  register(deps)

  t.deepEqual(await deps.get("foo"), ["barwaldo", "batwaldo"])
})

test('I can `resolve` a whole graph of deps', async t => {
  const { deps } = t.context

  const expected = register(deps)

  t.deepEqual(await deps.resolve(), expected)
})

test('I can `resolve` all deps into a `main` callback', t => {
  const { deps } = t.context

  const expected = register(deps)

  return t.notThrowsAsync(deps.main(resolved => {
    t.deepEqual(resolved, expected)
  }))
})

test('I can NOT `register` after calling `resolve`', t => {
  const { deps } = t.context

  register(deps)

  deps.resolve()

  t.throws(() => deps.register('anything', deps => true))
})

test('I can `replace` an `AlreadyRegistered` dep', async t => {
  const { deps } = t.context

  deps.register('API_BASE_URL', deps => '/api')

  t.throws(() => deps.register('API_BASE_URL', deps => '//localhost:9000/'))

  t.assert(await deps.get('API_BASE_URL') === '/api',
    'since `API_BASE_URL` has not changed')

  t.notThrows(() => {
    t.assert(deps === deps.replace('API_BASE_URL', deps => '//localhost:9000/'))
  })

  t.assert(await deps.get('API_BASE_URL') === '//localhost:9000/',
    'since `API_BASE_URL` has been `replace`-ed')
})

test('I can `decorate` an `AlreadyRegistered` dep', async t => {
  const { deps } = t.context

  deps.register('foo', deps => 'bar')

  deps.decorate('foo', (deps, original) => original + 'baz')

  deps.decorate('foo', (deps, original) => original + 'bat')

  t.assert(await deps.get('foo') === 'barbazbat')
})

test('If I `replace` a `decorated` dep, well...', async t => {
  const { deps } = t.context

  deps.register('foo', deps => 'bar')
    .decorate('foo', (deps, original) => original + 'baz')
    .replace('foo', deps => 'quux')

  t.assert(await deps.get('foo') === 'quux')
})

test.todo('Requesting Singletons: `deps.singleton`?')

/**
 * @param {deps.Injector} deps to seed; mutated
 * @return {Object<string:any>} expected resolved values
 */
function register(deps){
  deps.register("quux", deps => "waldo") // found him!

  deps.register(
    "foo",
    async deps => await Promise.all([deps.get("bar"), deps.get("bat")])
  )

  deps.register("bat", async deps => "bat" + (await deps.get("quux")))

  deps.register("bar", async deps => "bar" + (await deps.get("quux")))

  return {
    quux: "waldo",
    foo: ["barwaldo", "batwaldo"],
    bat: "batwaldo", // the hero we need
    bar: "barwaldo", // the one we deserve
  }
}
