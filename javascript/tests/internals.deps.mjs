import test from 'ava'
import beforeEach from './helpers.beforeEach'
import { CircularDependency } from '../deps'

test.beforeEach(beforeEach)

// Use with caution!
test('Injector.clone', async t => {
  const { deps } = t.context

  const clone = deps.clone()

  deps('A', deps => 'A')

  t.assert(await deps('A') === await clone('A'))
})

test('Injector~exposeForTesting via Injector.$$noop', t => {
  const { deps } = t.context

  t.assert(deps.noop === undefined,
    'since `exposeForTesting` renames it')

  t.assert(deps.$$noop instanceof Function,
    'since `exposeForTesting` wraps it in a Function')

  t.assert(process.env.NODE_ENV === 'test')

  t.notThrows(() => deps.$$noop(), 'since `NODE_ENV` is "test"')

  process.env.NODE_ENV = 'development'

  t.throws(() => deps.$$noop(),
    TypeError, 'since `NODE_ENV` is NOT "test"')

  process.env.NODE_ENV = 'test' // multithreading yay!
})

// Exposed only for testing...
test('Injector.$$proxyFor', async t => {
  const { deps } = t.context

  const proxy = deps.$$proxyFor(deps, 'foo')

  t.assert(proxy instanceof Function,
    'since `proxy` behaves just like `deps`')

  t.assert(deps !== proxy,
    'since `proxy` is a separate instance from `deps`')

  // 'bar' is registered on `deps`, not `proxy`
  deps('bar', deps => 'bar')

  // but "bar" is available on `proxy`, too
  t.assert(await deps('bar') === await proxy('bar'),
    'since `proxy` is a `clone` of `deps`')
})

// Used to track circular dependencies...
test('Injector.$$path', async t => {
  const { deps } = t.context

  const foo = deps.$$proxyFor(deps, 'foo')

  t.assert(deps.$$path === undefined,
    'since `deps` is a root injector')

  t.assert(foo.$$path !== undefined,
    'since `foo` is a `$$proxyFor` "foo" derived from `deps`')

  t.deepEqual(foo.$$path, ['foo'])

  t.throwsReadOnly = (...tests) => t => {
    tests.map(test => t.throws(test,
      TypeError, 'since `$$path` is read-only'))
  }

  t.throwsReadOnly(
    () => { foo.$$path = 'bar' },
    () => { foo.$$path.push('bar') },
    () => { foo.$$path.pop() },
  )

  let modified = foo.$$path.concat('bar')

  t.assert(modified.includes('foo') && modified.includes('bar'),
    'since `$$path` still supports `concat`')

  t.assert(!!!foo.$$path.includes('bar'),
    'since `concat` does not modify in-place')

  t.deepEqual([].concat(foo.$$path, 'bar'), ['foo', 'bar'],
    'since `Array.concat` treats `$$path` like an Array')

  const bar = foo.$$proxyFor(foo, 'bar')

  t.deepEqual(bar.$$path, ['foo', 'bar'],
    'since `bar` is a `$$proxyFor` "bar" derived from `foo`')

  const baz = deps.$$proxyFor(bar, 'baz')

  t.deepEqual(baz.$$path, ['foo', 'bar', 'baz'],
    'since `baz` is a `$$proxyFor` "baz" derived from `bar`')
})
