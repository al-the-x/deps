export class AlreadyResolved extends Error {
  constructor(name, ...extra){
    super(`Cannot register ${name} once resolved`, ...extra)
  }
}

export class AlreadyRegistered extends Error {
  constructor(name, ...extra) {
    super(`${name} is already registered`, ...extra)
  }
}

export class CircularDependency extends Error {
  constructor(names=[], ...extra) {
    super(`Circular dependency: ${names.join(' -> ')}`)
  }
}

export class NotRegistered extends Error {
  constructor(name, ...extra) {
    super(`${name} is not registered`, ...extra)
  }
}

export class NotSupported extends Error {
  constructor(args, ...extra){
    super(`Arguments supplied are not supported: ${args}`, ...extra)
  }
}

/** @module deps */
export default factory()

/** @constructs {Injector} */
function factory({ injector = new Map, resolved = new Map }={}){
  /** @lends Injector */
  const Injector = {
    decorate(name, callback) {
      if (!!!injector.has(name)) throw new NotRegistered

      const original = injector.get(name)

      injector.set(name, async deps => {
        return callback(deps, await original(deps))
      })

      return this
    },

    // @return {Injector} with reset registry and unresolved deps
    detach: () => factory(),

    // @return {Injector} preserving registered and resolved deps
    clone: () => factory({ injector, resolved }),

    // @throws {NotRegistered} if {name} has not been registered yet
    // @return {Promise<any>}
    async get(name) {
      if (Array.isArray(name)) {
        return Promise.all([].concat(name)
          .map(name => this.get(name)))
      }

      if (!injector.has(name)) throw new NotRegistered(name)

      childOf(resolved, this.$$path, name)

      const callback = injector.get(name)

      const proxy = proxyFor(this, name)

      return await callback(proxy)
    },

    async main(callback) {
      return callback(await this.resolve())
    },

    // @throws {AlreadyRegistered} if {name} is already registered
    register(name, callback) {
      if (resolved.done) throw new AlreadyResolved(name)

      if (injector.has(name)) {
        throw new AlreadyRegistered(name)
      }

      injector.set(name, callback)

      return this
    },

    // @return {Promise<Object<name:value>>}
    async resolve() {
      if (!resolved.done) {
        resolved.done = Promise.all(
          Array.from(injector.keys())
            .map(async name => [name, await this.get(name)])
        )
      }

      return fromEntries(await resolved.done)
    },

    /**
     * Replace registered `name` with `callback` unless {@link NotRegistered}
     *
     * @param {string} name to replace
     * @param {dep} callback
     * @return {Injector}
     * @throw {NotRegistered} if {@link Injector.register} has not seen `name`
     */
    replace(name, callback){
      if (!injector.has(name)) throw new NotRegistered(name)

      injector.set(name, callback)

      return this
    },
  }

  /** @lends deps */
  return Object.assign(deps, Injector, exposeForTesting(
    childOf,
    proxyFor,
    function noop(){},
  ))

  /**
   * @param {Map.<string,Set>} resolved deps
   * @param {string[]} parents of `child`
   * @param {string} child key to consider
   * @return {boolean} if `child` is a child of `parents`
   * @throws {CircularDependency} when a circular dependency is detected
   */
  function childOf(resolved, parents, child){
    // If `parents` is missing or empty, we're starting from root
    if (!parents || !parents.length) {
      resolved.set(child, new Set)

      return false // `child` is not a `childOf` supplied `parents`
    }

    /**
     * Check that `parents` does not contain `child`, e.g.
     *
     * - child: foo
     * - parents: [ foo, bar ]
     *
     * !!! CircularDependency: foo -> bar -> foo
     */
    if (parents.includes(child)){
      const path = parents.slice(parents.indexOf(child)).concat(child)

      throw new CircularDependency(path)
    }

    if (!!!resolved.has(child)) resolved.set(child, new Set)

    /**
     * Check that `parents` does not contain a `childOf` the `child`, e.g.
     *
     * - child: foo
     * - parents: [ bar, baz ]
     * - resolved: { foo: [ bar ] }
     *
     * !!! CircularDependency: bar -> baz -> foo -> bar
     */
    const visited = [ ]

    visited.concat(child).forEach(function visit(visiting){
      visited.push(visiting)

      const toVisit = (resolved.get(visiting) || new Set)

      const parent = parents.find(parent => toVisit.has(parent))

      if (parent){
        const path = parents.slice(parents.indexOf(parent)).concat(visited, parent)

        throw new CircularDependency(path)
      }

      /**
       * Check that `parents` does not contain a `childOf` any DESCENDANTS
       * of the `child`, e.g.
       *
       * - child: foo
       * - parents: [ bar, baz ]
       * - resolved: { foo: [ bat ], bat: [ baz ] }
       *
       * !!! CircularDependency: baz -> foo -> bat -> baz
       */
      toVisit.forEach(visit) // RECURSION

      visited.pop()
    })

    parents.map(parent => resolved.has(parent)
      ? resolved.get(parent).add(child)
      : resolved.set(parent, new Set([ child ]))
    )

    return true // child is assigned to last parent
  }

  /** @namespace deps */
  function deps(...args){
    switch (args.length) {
      case 0:
        return deps.resolve()

      case 1:
        return args[0] instanceof Function
          ? deps.main(args[0])
          : deps.get(args[0])

      case 2:
        const [ name, dep ] = args

        // In case someone gets confused...
        if (!Array.isArray(name)) {
          return deps.register(name, dep)
        }

      default:
        throw new NotSupported(args)
    }
  }

  /**
   * @param {Injector} deps to proxy
   * @param {string} name to append to {Injector.$$path}
   * @return {Injector}
   */
  function proxyFor(deps, name){
    const $$path = Object.freeze(
      [].concat(deps.$$path || [ ], name)
    )

    return Object.defineProperty(
      deps.clone(), '$$path', { value: $$path }
    )
  }
}

/**
 * @callback throwUnlessTesting
 * @param {...any} args passed to enclosed function
 * @return {any} per enclosed function
 * @throw {NotImplemented} if `NODE_ENV !== 'test'`
 */

/**
 * Meta-programming to expose certain `privateFns` for testing purposes
 *
 * @param {Function[]} privateFns hidden by closure
 * @return {Object.<string,throwUnlessTesting>} exposing `privateFns` only for testing
 */
function exposeForTesting(...privateFns){
  return privateFns
    .map(privateFn => [`$$${privateFn.name}`, privateFn])
    .reduce((methods, [name, method]) => Object.assign(methods, {
      [name]: (...args) => {
        if (process.env.NODE_ENV !== 'test') {
          throw new TypeError(`${name} is not a function`)
        }
        return method(...args)
      }
    }), { })
}

/**
 * Conditional polyfill for {Object.fromEntries}
 *
 * @param {[key,value][]} entries per {Object.fromEntries}
 * @return {Object<key:value>} from {entries}
 */
function fromEntries(entries) {
  if (Object.fromEntries) return Object.fromEntries(entries)

  return entries.reduce(
    (map, [name, value]) =>
      Object.assign(map, {
        [name]: value
      }),
    {}
  )
}
